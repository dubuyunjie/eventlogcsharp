﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EventLogCSharp
{
    class GLOBALMETHOD
    {
        public class classOfListViewRecords
        {
            public string a_eventID;
            public string b_timeGenerated;
            public string c_strWeek;
            public string d_duration;
            public string e_context;
            public string f_comment;

            /// <summary>
            /// 将结构转为字符串，以便写入文件。之间用<>分隔
            /// </summary>
            /// <param name="s">待转换的结构</param>
            /// <returns>转换后的字符串</returns>
            public static string ConvertStructToString(classOfListViewRecords s)
            {
                return s.a_eventID + "@" + s.b_timeGenerated + "@" + s.c_strWeek + "@" + s.d_duration + "@" + s.e_context + "@" + "@" + s.f_comment;    //倒数第二个是序号的占位符
            }

            public static classOfListViewRecords ConvertStringToStruct(string str)
            {
                classOfListViewRecords s = new classOfListViewRecords();
                string[] strArray = str.Split(new string[]{"@"},StringSplitOptions.None);
//                 if (strArray.Length != 5)
//                 {
//                     MessageBox.Show("哪里没有把空项转成“”");
//                     return s;
//                 }

                s.a_eventID = strArray[0];
                s.b_timeGenerated = strArray[1];
                s.c_strWeek = strArray[2];
                s.d_duration = strArray[3];
                s.e_context = strArray[4];
                //5是序号，不用写入文件
                s.f_comment = strArray.Length > 6 ? strArray[6] : "";
                return s;
            }

            public static string[] ConvertStructToStringArray(classOfListViewRecords s)
            {
                string str = ConvertStructToString(s);
                string[] strArray = str.Split(new string[] { "@" }, StringSplitOptions.None);
                return strArray;
            }

			/// <summary>
			/// 将字符串转换为DateTime
			/// </summary>
			/// <param name="str">字符串格式为2012-10-30 17:09:59</param>
			/// <returns>返回DateTime</returns>
			public static DateTime ConvertStringToDateTime(string str)
			{
				string[] strArray = str.Split(' ');
				if (strArray.Length != 2)
				{
					return new DateTime(0);
				}

				string[] strArrayDate = strArray[0].Split('-');
				string[] strArrayTime = strArray[1].Split(':');

				DateTime dt = new DateTime();
				try
				{
					 dt = new DateTime(Convert.ToInt32(strArrayDate[0]), Convert.ToInt32(strArrayDate[1]), Convert.ToInt32(strArrayDate[2]),
					Convert.ToInt32(strArrayTime[0]), Convert.ToInt32(strArrayTime[1]), Convert.ToInt32(strArrayTime[2]));
				}
				catch (System.Exception ex)
				{
					
				}
				return dt;
			}

        }
    }
}
