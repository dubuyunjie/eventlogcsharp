﻿namespace EventLogCSharp
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.listViewShow = new System.Windows.Forms.ListView();
			this.columnHeaderType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeaderTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeaderWeek = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeaderDuration = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeaderInfo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeaderNO = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeaderComment = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.labelOnOffTimes = new System.Windows.Forms.Label();
			this.labelTimeContinue = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.labelRunningTime = new System.Windows.Forms.Label();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.查看ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuFromFile = new System.Windows.Forms.ToolStripMenuItem();
			this.menuFromEventLog = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.menuTotal = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.menuOpenFile = new System.Windows.Forms.ToolStripMenuItem();
			this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuVersionLog = new System.Windows.Forms.ToolStripMenuItem();
			this.menuAbout = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripButtonFromFile = new System.Windows.Forms.ToolStripButton();
			this.toolStripButtonFromEventLog = new System.Windows.Forms.ToolStripButton();
			this.toolStripButtonTotal = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripSplitButtonUp = new System.Windows.Forms.ToolStripSplitButton();
			this.TSMI_UpPowerOn = new System.Windows.Forms.ToolStripMenuItem();
			this.TSMI_UpPowerDown = new System.Windows.Forms.ToolStripMenuItem();
			this.TSMI_UpPowerUpDown = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSplitButtonNext = new System.Windows.Forms.ToolStripSplitButton();
			this.TSMI_NextPowerOn = new System.Windows.Forms.ToolStripMenuItem();
			this.TSMI_NextPowerDown = new System.Windows.Forms.ToolStripMenuItem();
			this.TSMI_NextPowerUpDown = new System.Windows.Forms.ToolStripMenuItem();
			this.打开文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.asfsdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.lbPowerOn = new System.Windows.Forms.Label();
			this.lbPowerDown = new System.Windows.Forms.Label();
			this.lbSerial = new System.Windows.Forms.Label();
			this.menuStrip1.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// listViewShow
			// 
			this.listViewShow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.listViewShow.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderType,
            this.columnHeaderTime,
            this.columnHeaderWeek,
            this.columnHeaderDuration,
            this.columnHeaderInfo,
            this.columnHeaderNO,
            this.columnHeaderComment});
			this.listViewShow.FullRowSelect = true;
			this.listViewShow.Location = new System.Drawing.Point(0, 53);
			this.listViewShow.MultiSelect = false;
			this.listViewShow.Name = "listViewShow";
			this.listViewShow.Size = new System.Drawing.Size(702, 265);
			this.listViewShow.TabIndex = 1;
			this.listViewShow.UseCompatibleStateImageBehavior = false;
			this.listViewShow.View = System.Windows.Forms.View.Details;
			this.listViewShow.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewShow_MouseDoubleClick);
			// 
			// columnHeaderType
			// 
			this.columnHeaderType.Text = "类型";
			// 
			// columnHeaderTime
			// 
			this.columnHeaderTime.Text = "时间";
			this.columnHeaderTime.Width = 153;
			// 
			// columnHeaderWeek
			// 
			this.columnHeaderWeek.Text = "星期";
			this.columnHeaderWeek.Width = 68;
			// 
			// columnHeaderDuration
			// 
			this.columnHeaderDuration.Text = "持续时间";
			this.columnHeaderDuration.Width = 90;
			// 
			// columnHeaderInfo
			// 
			this.columnHeaderInfo.Text = "说明";
			this.columnHeaderInfo.Width = 101;
			// 
			// columnHeaderNO
			// 
			this.columnHeaderNO.Text = "序号";
			this.columnHeaderNO.Width = 37;
			// 
			// columnHeaderComment
			// 
			this.columnHeaderComment.Text = "备注";
			this.columnHeaderComment.Width = 638;
			// 
			// labelOnOffTimes
			// 
			this.labelOnOffTimes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.labelOnOffTimes.AutoSize = true;
			this.labelOnOffTimes.Location = new System.Drawing.Point(518, 341);
			this.labelOnOffTimes.Name = "labelOnOffTimes";
			this.labelOnOffTimes.Size = new System.Drawing.Size(65, 12);
			this.labelOnOffTimes.TabIndex = 2;
			this.labelOnOffTimes.Text = "开关机次数";
			// 
			// labelTimeContinue
			// 
			this.labelTimeContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelTimeContinue.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelTimeContinue.ForeColor = System.Drawing.Color.RoyalBlue;
			this.labelTimeContinue.Location = new System.Drawing.Point(130, 341);
			this.labelTimeContinue.Name = "labelTimeContinue";
			this.labelTimeContinue.Size = new System.Drawing.Size(156, 14);
			this.labelTimeContinue.TabIndex = 3;
			this.labelTimeContinue.Text = "000";
			this.labelTimeContinue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
			this.label1.Location = new System.Drawing.Point(38, 339);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(82, 14);
			this.label1.TabIndex = 4;
			this.label1.Text = "总运行时长";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.label2.ForeColor = System.Drawing.Color.Green;
			this.label2.Location = new System.Drawing.Point(38, 321);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(82, 14);
			this.label2.TabIndex = 5;
			this.label2.Text = "开机已运行";
			// 
			// labelRunningTime
			// 
			this.labelRunningTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelRunningTime.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.labelRunningTime.ForeColor = System.Drawing.Color.Green;
			this.labelRunningTime.Location = new System.Drawing.Point(130, 321);
			this.labelRunningTime.Name = "labelRunningTime";
			this.labelRunningTime.Size = new System.Drawing.Size(156, 14);
			this.labelRunningTime.TabIndex = 6;
			this.labelRunningTime.Text = "nul";
			this.labelRunningTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// timer1
			// 
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.timer_show);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查看ToolStripMenuItem,
            this.关于ToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(702, 24);
			this.menuStrip1.TabIndex = 7;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// 查看ToolStripMenuItem
			// 
			this.查看ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFromFile,
            this.menuFromEventLog,
            this.toolStripSeparator1,
            this.menuTotal,
            this.toolStripSeparator2,
            this.menuOpenFile});
			this.查看ToolStripMenuItem.Name = "查看ToolStripMenuItem";
			this.查看ToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
			this.查看ToolStripMenuItem.Text = "查看";
			// 
			// menuFromFile
			// 
			this.menuFromFile.CheckOnClick = true;
			this.menuFromFile.Image = ((System.Drawing.Image)(resources.GetObject("menuFromFile.Image")));
			this.menuFromFile.Name = "menuFromFile";
			this.menuFromFile.Size = new System.Drawing.Size(166, 22);
			this.menuFromFile.Text = "文件中的记录";
			this.menuFromFile.ToolTipText = "保存在文件中的记录";
			this.menuFromFile.Click += new System.EventHandler(this.menuFromFile_Click);
			// 
			// menuFromEventLog
			// 
			this.menuFromEventLog.CheckOnClick = true;
			this.menuFromEventLog.Image = ((System.Drawing.Image)(resources.GetObject("menuFromEventLog.Image")));
			this.menuFromEventLog.Name = "menuFromEventLog";
			this.menuFromEventLog.Size = new System.Drawing.Size(166, 22);
			this.menuFromEventLog.Text = "事件日志中的记录";
			this.menuFromEventLog.ToolTipText = "事件日志中的记录";
			this.menuFromEventLog.Click += new System.EventHandler(this.menuFromEventLog_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(163, 6);
			// 
			// menuTotal
			// 
			this.menuTotal.Checked = true;
			this.menuTotal.CheckOnClick = true;
			this.menuTotal.CheckState = System.Windows.Forms.CheckState.Checked;
			this.menuTotal.Image = ((System.Drawing.Image)(resources.GetObject("menuTotal.Image")));
			this.menuTotal.Name = "menuTotal";
			this.menuTotal.Size = new System.Drawing.Size(166, 22);
			this.menuTotal.Text = "二者合并后的记录";
			this.menuTotal.ToolTipText = "合并后的记录";
			this.menuTotal.Click += new System.EventHandler(this.menuTotal_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(163, 6);
			// 
			// menuOpenFile
			// 
			this.menuOpenFile.Name = "menuOpenFile";
			this.menuOpenFile.Size = new System.Drawing.Size(166, 22);
			this.menuOpenFile.Text = "打开文件";
			this.menuOpenFile.Click += new System.EventHandler(this.menuOpenFile_Click);
			// 
			// 关于ToolStripMenuItem
			// 
			this.关于ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuVersionLog,
            this.menuAbout});
			this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
			this.关于ToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
			this.关于ToolStripMenuItem.Text = "帮助";
			// 
			// menuVersionLog
			// 
			this.menuVersionLog.Image = ((System.Drawing.Image)(resources.GetObject("menuVersionLog.Image")));
			this.menuVersionLog.Name = "menuVersionLog";
			this.menuVersionLog.Size = new System.Drawing.Size(118, 22);
			this.menuVersionLog.Text = "版本日志";
			this.menuVersionLog.Click += new System.EventHandler(this.menuVersionLog_Click);
			// 
			// menuAbout
			// 
			this.menuAbout.Image = ((System.Drawing.Image)(resources.GetObject("menuAbout.Image")));
			this.menuAbout.Name = "menuAbout";
			this.menuAbout.Size = new System.Drawing.Size(118, 22);
			this.menuAbout.Text = "关于";
			this.menuAbout.Click += new System.EventHandler(this.menuAbout_Click);
			// 
			// toolStrip1
			// 
			this.toolStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonFromFile,
            this.toolStripButtonFromEventLog,
            this.toolStripButtonTotal,
            this.toolStripSeparator4,
            this.toolStripSplitButtonUp,
            this.toolStripSplitButtonNext});
			this.toolStrip1.Location = new System.Drawing.Point(0, 24);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(702, 25);
			this.toolStrip1.TabIndex = 8;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// toolStripButtonFromFile
			// 
			this.toolStripButtonFromFile.CheckOnClick = true;
			this.toolStripButtonFromFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButtonFromFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFromFile.Image")));
			this.toolStripButtonFromFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonFromFile.Name = "toolStripButtonFromFile";
			this.toolStripButtonFromFile.Size = new System.Drawing.Size(23, 22);
			this.toolStripButtonFromFile.Text = "显示文件中的记录";
			this.toolStripButtonFromFile.Click += new System.EventHandler(this.menuFromFile_Click);
			// 
			// toolStripButtonFromEventLog
			// 
			this.toolStripButtonFromEventLog.CheckOnClick = true;
			this.toolStripButtonFromEventLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButtonFromEventLog.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonFromEventLog.Image")));
			this.toolStripButtonFromEventLog.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonFromEventLog.Name = "toolStripButtonFromEventLog";
			this.toolStripButtonFromEventLog.Size = new System.Drawing.Size(23, 22);
			this.toolStripButtonFromEventLog.Text = "显示事件日志中的记录";
			this.toolStripButtonFromEventLog.Click += new System.EventHandler(this.menuFromEventLog_Click);
			// 
			// toolStripButtonTotal
			// 
			this.toolStripButtonTotal.Checked = true;
			this.toolStripButtonTotal.CheckOnClick = true;
			this.toolStripButtonTotal.CheckState = System.Windows.Forms.CheckState.Checked;
			this.toolStripButtonTotal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButtonTotal.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonTotal.Image")));
			this.toolStripButtonTotal.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonTotal.Name = "toolStripButtonTotal";
			this.toolStripButtonTotal.Size = new System.Drawing.Size(23, 22);
			this.toolStripButtonTotal.Text = "显示合并后的记录";
			this.toolStripButtonTotal.Click += new System.EventHandler(this.menuTotal_Click);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripSplitButtonUp
			// 
			this.toolStripSplitButtonUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripSplitButtonUp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_UpPowerOn,
            this.TSMI_UpPowerDown,
            this.TSMI_UpPowerUpDown});
			this.toolStripSplitButtonUp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButtonUp.Image")));
			this.toolStripSplitButtonUp.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripSplitButtonUp.Name = "toolStripSplitButtonUp";
			this.toolStripSplitButtonUp.Size = new System.Drawing.Size(34, 22);
			this.toolStripSplitButtonUp.Text = "上一个异常开关机";
			this.toolStripSplitButtonUp.ToolTipText = "跳转到上一个异常开关机";
			this.toolStripSplitButtonUp.ButtonClick += new System.EventHandler(this.TSSBU_ButtonClick);
			// 
			// TSMI_UpPowerOn
			// 
			this.TSMI_UpPowerOn.Image = ((System.Drawing.Image)(resources.GetObject("TSMI_UpPowerOn.Image")));
			this.TSMI_UpPowerOn.Name = "TSMI_UpPowerOn";
			this.TSMI_UpPowerOn.Size = new System.Drawing.Size(166, 22);
			this.TSMI_UpPowerOn.Text = "上一个异常开机";
			this.TSMI_UpPowerOn.Click += new System.EventHandler(this.TSMI_UpPowerOn_Click);
			// 
			// TSMI_UpPowerDown
			// 
			this.TSMI_UpPowerDown.Image = ((System.Drawing.Image)(resources.GetObject("TSMI_UpPowerDown.Image")));
			this.TSMI_UpPowerDown.Name = "TSMI_UpPowerDown";
			this.TSMI_UpPowerDown.Size = new System.Drawing.Size(166, 22);
			this.TSMI_UpPowerDown.Text = "上一个异常关机";
			this.TSMI_UpPowerDown.Click += new System.EventHandler(this.TSMI_UpPowerDown_Click);
			// 
			// TSMI_UpPowerUpDown
			// 
			this.TSMI_UpPowerUpDown.Image = ((System.Drawing.Image)(resources.GetObject("TSMI_UpPowerUpDown.Image")));
			this.TSMI_UpPowerUpDown.Name = "TSMI_UpPowerUpDown";
			this.TSMI_UpPowerUpDown.Size = new System.Drawing.Size(166, 22);
			this.TSMI_UpPowerUpDown.Text = "上一个异常开关机";
			this.TSMI_UpPowerUpDown.Click += new System.EventHandler(this.TSMI_UpPowerUpDown_Click);
			// 
			// toolStripSplitButtonNext
			// 
			this.toolStripSplitButtonNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripSplitButtonNext.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_NextPowerOn,
            this.TSMI_NextPowerDown,
            this.TSMI_NextPowerUpDown});
			this.toolStripSplitButtonNext.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButtonNext.Image")));
			this.toolStripSplitButtonNext.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripSplitButtonNext.Name = "toolStripSplitButtonNext";
			this.toolStripSplitButtonNext.Size = new System.Drawing.Size(34, 22);
			this.toolStripSplitButtonNext.Text = "下一个异常开关机";
			this.toolStripSplitButtonNext.ToolTipText = "跳转到下一个异常开关机处";
			this.toolStripSplitButtonNext.ButtonClick += new System.EventHandler(this.TSSBN_ButtonClick);
			// 
			// TSMI_NextPowerOn
			// 
			this.TSMI_NextPowerOn.Image = ((System.Drawing.Image)(resources.GetObject("TSMI_NextPowerOn.Image")));
			this.TSMI_NextPowerOn.Name = "TSMI_NextPowerOn";
			this.TSMI_NextPowerOn.Size = new System.Drawing.Size(166, 22);
			this.TSMI_NextPowerOn.Text = "下一个异常开机";
			this.TSMI_NextPowerOn.Click += new System.EventHandler(this.TSMI_NextPowerOn_Click);
			// 
			// TSMI_NextPowerDown
			// 
			this.TSMI_NextPowerDown.Image = ((System.Drawing.Image)(resources.GetObject("TSMI_NextPowerDown.Image")));
			this.TSMI_NextPowerDown.Name = "TSMI_NextPowerDown";
			this.TSMI_NextPowerDown.Size = new System.Drawing.Size(166, 22);
			this.TSMI_NextPowerDown.Text = "下一个异常关机";
			this.TSMI_NextPowerDown.Click += new System.EventHandler(this.TSMI_NextPowerDown_Click);
			// 
			// TSMI_NextPowerUpDown
			// 
			this.TSMI_NextPowerUpDown.Image = ((System.Drawing.Image)(resources.GetObject("TSMI_NextPowerUpDown.Image")));
			this.TSMI_NextPowerUpDown.Name = "TSMI_NextPowerUpDown";
			this.TSMI_NextPowerUpDown.Size = new System.Drawing.Size(166, 22);
			this.TSMI_NextPowerUpDown.Text = "下一个异常开关机";
			this.TSMI_NextPowerUpDown.Click += new System.EventHandler(this.TSMI_NextPowerUpDown_Click);
			// 
			// 打开文件ToolStripMenuItem
			// 
			this.打开文件ToolStripMenuItem.Name = "打开文件ToolStripMenuItem";
			this.打开文件ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
			this.打开文件ToolStripMenuItem.Text = "打开文件";
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(169, 6);
			// 
			// asfsdfToolStripMenuItem
			// 
			this.asfsdfToolStripMenuItem.Name = "asfsdfToolStripMenuItem";
			this.asfsdfToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
			this.asfsdfToolStripMenuItem.Text = "asfsdf";
			this.asfsdfToolStripMenuItem.Click += new System.EventHandler(this.menuOpenFile_Click);
			// 
			// lbPowerOn
			// 
			this.lbPowerOn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lbPowerOn.AutoSize = true;
			this.lbPowerOn.ForeColor = System.Drawing.Color.Green;
			this.lbPowerOn.Location = new System.Drawing.Point(339, 323);
			this.lbPowerOn.Name = "lbPowerOn";
			this.lbPowerOn.Size = new System.Drawing.Size(101, 12);
			this.lbPowerOn.TabIndex = 2;
			this.lbPowerOn.Text = "非正常开机次数：";
			// 
			// lbPowerDown
			// 
			this.lbPowerDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lbPowerDown.AutoSize = true;
			this.lbPowerDown.ForeColor = System.Drawing.Color.Red;
			this.lbPowerDown.Location = new System.Drawing.Point(339, 341);
			this.lbPowerDown.Name = "lbPowerDown";
			this.lbPowerDown.Size = new System.Drawing.Size(101, 12);
			this.lbPowerDown.TabIndex = 2;
			this.lbPowerDown.Text = "非正常关机次数：";
			// 
			// lbSerial
			// 
			this.lbSerial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lbSerial.AutoSize = true;
			this.lbSerial.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.lbSerial.Location = new System.Drawing.Point(518, 323);
			this.lbSerial.Name = "lbSerial";
			this.lbSerial.Size = new System.Drawing.Size(11, 12);
			this.lbSerial.TabIndex = 2;
			this.lbSerial.Text = ".";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(702, 366);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.labelRunningTime);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.labelTimeContinue);
			this.Controls.Add(this.lbPowerDown);
			this.Controls.Add(this.lbPowerOn);
			this.Controls.Add(this.lbSerial);
			this.Controls.Add(this.labelOnOffTimes);
			this.Controls.Add(this.listViewShow);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "开关机详细信息";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView listViewShow;
		private System.Windows.Forms.ColumnHeader columnHeaderType;
		private System.Windows.Forms.ColumnHeader columnHeaderTime;
		private System.Windows.Forms.ColumnHeader columnHeaderDuration;
		private System.Windows.Forms.ColumnHeader columnHeaderWeek;
		private System.Windows.Forms.Label labelOnOffTimes;
		private System.Windows.Forms.Label labelTimeContinue;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label labelRunningTime;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ColumnHeader columnHeaderInfo;
		private System.Windows.Forms.ColumnHeader columnHeaderNO;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem 查看ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem menuFromFile;
		private System.Windows.Forms.ToolStripMenuItem menuFromEventLog;
		private System.Windows.Forms.ToolStripMenuItem menuTotal;
		private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem menuVersionLog;
		private System.Windows.Forms.ToolStripMenuItem menuAbout;
        private System.Windows.Forms.ColumnHeader columnHeaderComment;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonFromFile;
        private System.Windows.Forms.ToolStripButton toolStripButtonFromEventLog;
        private System.Windows.Forms.ToolStripButton toolStripButtonTotal;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 打开文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menuOpenFile;
        private System.Windows.Forms.ToolStripMenuItem asfsdfToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButtonUp;
        private System.Windows.Forms.ToolStripMenuItem TSMI_UpPowerOn;
        private System.Windows.Forms.ToolStripMenuItem TSMI_UpPowerDown;
        private System.Windows.Forms.ToolStripMenuItem TSMI_UpPowerUpDown;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButtonNext;
        private System.Windows.Forms.ToolStripMenuItem TSMI_NextPowerOn;
        private System.Windows.Forms.ToolStripMenuItem TSMI_NextPowerDown;
        private System.Windows.Forms.ToolStripMenuItem TSMI_NextPowerUpDown;
		private System.Windows.Forms.Label lbPowerOn;
		private System.Windows.Forms.Label lbPowerDown;
		private System.Windows.Forms.Label lbSerial;
	}
}

