﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EventLogCSharp
{
    public partial class FormInput : Form
    {
        public FormInput(string strComm)
        {
            InitializeComponent();

            textBoxComment.Text = strComm;
            textBoxComment.Select();
        }

        public string strComment;
        private void buttonOK_Click(object sender, EventArgs e)
        {
            strComment = textBoxComment.Text;
            strComment = strComment.Replace('@', '＠');
            this.DialogResult = DialogResult.OK;
            //this.Close();
        }
    }
}
